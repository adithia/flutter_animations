import 'package:flutter/material.dart';
import 'package:flutter_animations/pages/signIn.dart';



void main(){
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false, 
      home: SignInPage(),
    )
  );
}
