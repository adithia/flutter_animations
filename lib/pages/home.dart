import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter_animations/components/horizontal_listview.dart';
import 'package:flutter_animations/components/products.dart';
import 'package:flutter_animations/pages/cart.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    //slide image
    Widget imageCarousel = new Container(
      height: 200.0,
      child: new Carousel(
        boxFit: BoxFit.cover,
        images: [
          AssetImage('images/slider/c1.jpg'),
          AssetImage('images/slider/m1.jpeg'),
          AssetImage('images/slider/m2.jpg'),
          AssetImage('images/slider/w1.jpeg'),
          AssetImage('images/slider/w3.jpeg'),
          AssetImage('images/slider/w4.jpeg'),
        ],
        autoplay: true,
        // animationCurve: Curves.fastOutSlowIn,
        // animationDuration: Duration(milliseconds: 1000),
        dotSize: 3.0, dotColor: Colors.grey,
        indicatorBgPadding: 2.0,
        dotBgColor: Colors.transparent,
      ),
    );

    //toolbar home
    return Scaffold(
      appBar: new AppBar(
        elevation: 0.1,
        title: Container(
            margin: EdgeInsets.only(left: 40),
            child: Center(child: Text('Qreseqin'))),
        backgroundColor: Colors.red[700],
        actions: <Widget>[
          new IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {},
          ),
          new IconButton(
            icon: Icon(
              Icons.shopping_cart,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => new Cart()));
            },
          )
        ],
      ),

      //leftbar navigator
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.transparent,
        ),
        child: new Drawer(
          child: new ListView(
            children: <Widget>[
              new UserAccountsDrawerHeader(
                accountName: Text('Qreseqin'),
                accountEmail: Text('qreseqin@qreseqin.com'),
                currentAccountPicture: GestureDetector(
                  child: new CircleAvatar(
                    backgroundColor: Colors.grey[200],
                    child: Icon(
                      Icons.person,
                      color: Colors.white,
                    ),
                  ),
                ),
                decoration: new BoxDecoration(color: Colors.transparent),
              ),

              //list leftbar menu
              InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => new HomePage()));
                },
                child: ListTile(
                  title: Text(
                    'Home Page',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(
                    Icons.home,
                    color: Colors.white,
                  ),
                ),
              ),
              InkWell(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    'My Account',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                ),
              ),
              InkWell(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    'My Order',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(
                    Icons.shopping_basket,
                    color: Colors.white,
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => new Cart()));
                },
                child: ListTile(
                  title: Text(
                    'Shooping Cart',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(
                    Icons.shopping_cart,
                    color: Colors.white,
                  ),
                ),
              ),
              InkWell(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    'Favorite',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(
                    Icons.favorite,
                    color: Colors.white,
                  ),
                ),
              ),
              InkWell(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    'Settings',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(
                    Icons.settings,
                    color: Colors.white,
                  ),
                ),
              ),
              InkWell(
                onTap: () {},
                child: ListTile(
                  title: Text(
                    'About',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(
                    Icons.help,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),

      body: new Column(
        children: <Widget>[
          imageCarousel,
          new Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                alignment: Alignment.centerLeft,
                child: new Text(
                  'Categories',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.red[700]),
                )),
          ),
          HorizontalList(),
          new Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                alignment: Alignment.centerLeft,
                child: new Text(
                  'Product',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.red[700]),
                )),
          ),
          Flexible(child: Products()),
        ],
      ),
    );
  }
}
