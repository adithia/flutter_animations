import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_animations/animations/signinAnimation.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key key}) : super(key: key);
  @override
  _SignInPageState createState() => new _SignInPageState();
}

class _SignInPageState extends State<SignInPage> with TickerProviderStateMixin {
  var statusClick = 0;

  TextEditingController editingControllerUser;
  TextEditingController editingControllerPass;
  AnimationController animationControllerButton;

  @override
  void initState() {
    editingControllerUser = new TextEditingController(text: '');
    editingControllerPass = new TextEditingController(text: '');

    super.initState();
    animationControllerButton =
        AnimationController(duration: Duration(seconds: 3), vsync: this)
          ..addStatusListener((status) {
            if (status == AnimationStatus.dismissed) {
              setState(() {
                statusClick = 0;
              });
            }
          });
  }

  @override
  void dispose() {
    super.dispose();
    animationControllerButton.dispose();
  }

  Future<Null> _playAnimation() async {
    try {
      await animationControllerButton.forward();
      await animationControllerButton.reverse();
    } on TickerCanceled {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/background/background3.jpg'),
              fit: BoxFit.cover),
        ),
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                Color.fromRGBO(162, 146, 199, 0.8),
                Color.fromRGBO(51, 51, 63, 0.9)
              ],
                  begin: FractionalOffset.topCenter,
                  end: FractionalOffset.bottomCenter)),
          child: ListView(
            padding: const EdgeInsets.all(0.0),
            children: <Widget>[
              Stack(
                alignment: AlignmentDirectional.bottomCenter,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 260.0),
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: TextField(
                                    keyboardType: TextInputType.emailAddress,
                                    controller: editingControllerUser,
                                    decoration: InputDecoration(
                                        labelText: "Username",
                                        icon: Icon(
                                          Icons.account_circle,
                                          color: Colors.red[700],),
                                        ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: TextField(
                                    keyboardType: TextInputType.text,
                                    controller: editingControllerPass,
                                    obscureText: true,
                                    decoration: InputDecoration(
                                        labelText: "Password",
                                        icon: Icon(Icons.lock,
                                            color: Colors.red[700]),
                                        ),
                                  ),
                                ),
                                FlatButton(
                                  padding: const EdgeInsets.only(
                                      top: 400.0, bottom: 30.0),
                                  onPressed: null,
                                  child: Text(
                                      "Don't have an account? Sign Up Here",
                                      style: TextStyle(
                                          fontSize: 14.0,
                                          color: Colors.white,
                                          fontWeight: FontWeight.w300,
                                          letterSpacing: 0.5)),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  statusClick == 0
                      ? new InkWell(
                          onTap: () {
                            setState(() {
                              statusClick = 1;
                            });
                            _playAnimation();
                          },
                          child: new SignIn())
                      : new SignInAnimation(
                          buttonController: animationControllerButton.view,
                          user: editingControllerUser.text,
                          pass: editingControllerPass.text,
                        )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

class SignIn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(60.0),
      child: new Container(
        alignment: FractionalOffset.center,
        width: 320.0,
        height: 60.0,
        decoration: BoxDecoration(
            color: Colors.red[700],
            borderRadius: BorderRadius.all(const Radius.circular(30.0))),
        child: Text('Sign In',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.3,
            )),
      ),
    );
  }
}
