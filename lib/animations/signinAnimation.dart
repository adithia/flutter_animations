import 'package:flutter/material.dart';
import 'package:flutter_animations/pages/home.dart';

class SignInAnimation extends StatefulWidget {
  SignInAnimation({Key key, this.buttonController, this.user,this.pass})
      : shirkButtonAnimation = new Tween(
          begin: 320.0,
          end: 70.0,
        ).animate(
            CurvedAnimation(
                parent: buttonController, curve: Interval(0.0, 0.150)),
            ),

            zoomAnimation = new Tween(
              begin: 70.0,
              end: 900.0,
            ).animate(CurvedAnimation(
              parent: buttonController,
              curve : Interval(
                0.550,
                0.999,
                curve: Curves.bounceInOut,
              )
            )),
        super(key: key);

  final AnimationController buttonController;
  final Animation shirkButtonAnimation;
  final Animation zoomAnimation;
  
  final String user;
  final String pass;

  Widget _buildAnimation(BuildContext context, Widget child) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 60.0),
      child: zoomAnimation.value <=300?
      new Container(
          alignment: FractionalOffset.center,
          width: shirkButtonAnimation.value,
          height: 60.0,
          decoration: BoxDecoration(
              color: Colors.red[700],
              borderRadius: BorderRadius.all(const Radius.circular(30.0))),
          child: shirkButtonAnimation.value > 75
              ? Text(
                  'Sign In',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 0.3,
                  ),
                )
              : CircularProgressIndicator(
                  strokeWidth: 1.0,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ))
                : user == 'idr' ? 
                Container(
                   
                  width: zoomAnimation.value,
                  height: zoomAnimation.value,
                  decoration: BoxDecoration(
                    shape: zoomAnimation.value <600
                    ? BoxShape.circle
                    : BoxShape.rectangle,
                    color: Colors.red[700]
                  ),
                )
                : new Container(
          alignment: FractionalOffset.center,
          width: shirkButtonAnimation.value,
          height: 60.0,
          decoration: BoxDecoration(
              color: Colors.red[700],
              borderRadius: BorderRadius.all(const Radius.circular(30.0))),
          child: shirkButtonAnimation.value > 75
              ? Text(
                  'Sign In',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 0.3,
                  ),
                )
              : CircularProgressIndicator(
                  strokeWidth: 1.0,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ))
    );
  }

  @override
  _SignInAnimationState createState() => new _SignInAnimationState();
}

class _SignInAnimationState extends State<SignInAnimation> {



  @override
  Widget build(BuildContext context) {
    widget.buttonController.addListener((){
      if(widget.zoomAnimation.isCompleted){
        if(widget.user == 'idr'){
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context)=> HomePage(
              )
            )
          );
        }
      }
    });
    return new AnimatedBuilder(
      builder: widget._buildAnimation,
      animation: widget.buttonController,
    );
  }
}
