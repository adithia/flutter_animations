import 'package:flutter/material.dart';

class CartProducts extends StatefulWidget {
  @override
  _CartProductsState createState() => _CartProductsState();
}

class _CartProductsState extends State<CartProducts> {
  var productsonCart = [
    {
      "name": "Mans Blazer",
      "picture": "images/products/blazer1.jpeg",
      "price": 100.000,
      "size": "M",
      "color": "Black",
      "quantity": "1",
    },
    {
      "name": "Women Blazer",
      "picture": "images/products/blazer2.jpeg",
      "price": 120.000,
      "size": "S",
      "color": "Grey",
      "quantity": "1",
    },
    {
      "name": "Sporty Pants",
      "picture": "images/products/pants2.jpeg",
      "price": 80.000,
      "size": "32",
      "color": "Grey",
      "quantity": "1",
    },
    {
      "name": "Mans Blazer",
      "picture": "images/products/blazer1.jpeg",
      "price": 120.000,
      "size": "32",
      "color": "Black",
      "quantity": "1",
    },
     {
      "name": "Red Dress",
      "picture": "images/products/dress1.jpeg",
      "price": 140.000,
      "size": "34",
      "color": "Red",
      "quantity": "1",
    },
  ];

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
        itemCount: productsonCart.length,
        itemBuilder: (context, index) {
          return SingleCartProduct(
            cartProdName: productsonCart[index]["name"],
            cartProdColor: productsonCart[index]["color"],
            cartProdQty: productsonCart[index]["quantity"],
            cartProdSize: productsonCart[index]["size"],
            cartProdPrice: productsonCart[index]["price"],
            cartProdPicture: productsonCart[index]["picture"],
            
          );
        });
  }
}
 
class SingleCartProduct extends StatelessWidget {
  final cartProdName;
  final cartProdPicture;
  final cartProdPrice;
  final cartProdSize;
  final cartProdColor;
  final cartProdQty;

  SingleCartProduct({
    this.cartProdName,
    this.cartProdPicture,
    this.cartProdPrice,
    this.cartProdSize,
    this.cartProdColor,
    this.cartProdQty,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
        child: ListTile(
          leading: new Image.asset(
            cartProdPicture,
            width: 80.0,
            height: 80.0,
          ),
          title: new Text(cartProdName),
          subtitle: new Column(
            children: <Widget>[
                  new Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: new Text("Size:"),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: new Text(
                          cartProdSize,
                          style: TextStyle(color: Colors.red[700]),
                        ),
                      ),
                      new Padding(
                        padding: const EdgeInsets.fromLTRB(
                          20.0,
                          8.0,
                          8.0,
                          8.0,
                        ),
                        child: new Text("Color:"),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: new Text(
                          cartProdColor,
                          style: TextStyle(color: Colors.red[700]),
                        ),
                      ),
                    ],
                  ),
                  new Container(
                    alignment: Alignment.topLeft,
                    child: new Text(
                      "\$$cartProdPrice",
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.red[700],
                      ),
                    ),
                  ),
                ],
          ),
         trailing: new Column(
             children: <Widget>[
               Expanded(child: new IconButton(icon: Icon(Icons.arrow_drop_up), onPressed: (){})),
               Expanded(child: Container(margin :EdgeInsets.only(top:10),child: new Text("$cartProdQty",))),
               Expanded(child: new IconButton(icon: Icon(Icons.arrow_drop_down ), onPressed:(){},))
             ],
           ),
          ),
    );
  }
}