import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_animations/pages/productDetails.dart';

class Products extends StatefulWidget {
  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  var productList = [
    {
      "name": "Mans Blazer",
      "picture": "images/products/blazer1.jpeg",
      "old_price": 120.000,
      "price": 100.000,
    },
    {
      "name": "Red Dress",
      "picture": "images/products/dress1.jpeg",
      "old_price": 140.000,
      "price": 100.000,
    },
    {
      "name": "Womens Blazer",
      "picture": "images/products/blazer2.jpeg",
      "old_price": 170.000,
      "price": 120.000,
    },
    {
      "name": "Black Dress",
      "picture": "images/products/dress2.jpeg",
      "old_price": 180.000,
      "price": 130.000,
    },
    {
      "name": "Daily Pants",
      "picture": "images/products/pants1.jpg",
      "old_price": 100.000,
      "price": 70.000,
    },
    {
      "name": "Sporty pants",
      "picture": "images/products/pants2.jpeg",
      "old_price": 110.000,
      "price": 80.000,
    },
    {
      "name": "Daily Skirt",
      "picture": "images/products/skt1.jpeg",
      "old_price": 150.000,
      "price": 70.000,
    },
    {
      "name": "Skirt Pink",
      "picture": "images/products/skt2.jpeg",
      "old_price": 120.000,
      "price": 80.000,
    },
     {
      "name": "Mans Blazer",
      "picture": "images/products/blazer1.jpeg",
      "old_price": 120.000,
      "price": 100.000,
    },
    {
      "name": "Red Dress",
      "picture": "images/products/dress1.jpeg",
      "old_price": 140.000,
      "price": 100.000,
    },
    {
      "name": "Womens Blazer",
      "picture": "images/products/blazer2.jpeg",
      "old_price": 170.000,
      "price": 120.000,
    },
    {
      "name": "Black Dress",
      "picture": "images/products/dress2.jpeg",
      "old_price": 180.000,
      "price": 130.000,
    },
    {
      "name": "Daily Pants",
      "picture": "images/products/pants1.jpg",
      "old_price": 100.000,
      "price": 70.000,
    },
    {
      "name": "Sporty pants",
      "picture": "images/products/pants2.jpeg",
      "old_price": 110.000,
      "price": 80.000,
    },
    {
      "name": "Daily Skirt",
      "picture": "images/products/skt1.jpeg",
      "old_price": 150.000,
      "price": 70.000,
    },
    {
      "name": "Skirt Pink",
      "picture": "images/products/skt2.jpeg",
      "old_price": 120.000,
      "price": 80.000,
    },
     {
      "name": "Mans Blazer",
      "picture": "images/products/blazer1.jpeg",
      "old_price": 120.000,
      "price": 100.000,
    },
    {
      "name": "Red Dress",
      "picture": "images/products/dress1.jpeg",
      "old_price": 140.000,
      "price": 100.000,
    },
    {
      "name": "Womens Blazer",
      "picture": "images/products/blazer2.jpeg",
      "old_price": 170.000,
      "price": 120.000,
    },
    {
      "name": "Black Dress",
      "picture": "images/products/dress2.jpeg",
      "old_price": 180.000,
      "price": 130.000,
    },
    {
      "name": "Daily Pants",
      "picture": "images/products/pants1.jpg",
      "old_price": 100.000,
      "price": 70.000,
    },
    {
      "name": "Sporty pants",
      "picture": "images/products/pants2.jpeg",
      "old_price": 110.000,
      "price": 80.000,
    },
    {
      "name": "Daily Skirt",
      "picture": "images/products/skt1.jpeg",
      "old_price": 150.000,
      "price": 70.000,
    },
    {
      "name": "Skirt Pink",
      "picture": "images/products/skt2.jpeg",
      "old_price": 120.000,
      "price": 80.000,
    }
    
  ];

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: productList.length,
        gridDelegate:
            new SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.all(4.0),
            child: SingleProd(
              prodName: productList[index]['name'],
              prodPicture: productList[index]['picture'],
              prodOldPrice: productList[index]['old_price'],
              prodPrice: productList[index]['price'],
            ),
          );
        });
  }
}

class SingleProd extends StatelessWidget {
  final prodName;
  final prodPicture;
  final prodOldPrice;
  final prodPrice;

  SingleProd({
    this.prodName,
    this.prodPicture,
    this.prodOldPrice,
    this.prodPrice,
  });

  
@override
  Widget build(BuildContext context) {
    return Card(
      child: Hero(
        tag: new Text("hero 1"),
        child: Material(
          child: InkWell(
            onTap: ()=>Navigator.of(context).push(new MaterialPageRoute(
              builder: (context)=> new ProductDetails(
                    productDetailName: prodName,
                    productDetailPicture: prodPicture,
                    productDetailOldPrice: prodOldPrice,
                    productDetailPrice: prodPrice,
              )
            )),
          child: GridTile( 
            footer: Container(
              color: Colors.white70,
              child: new Row(
                children: <Widget>[
                  Expanded(
                    child: Text(prodName, 
                    style: TextStyle(color: Colors.red[700], 
                    fontWeight: FontWeight.bold, fontSize: 16.0),),
                      ),
                      Container(margin :EdgeInsets.only(right:10),
                          child: Center(
                          child: new Text("\$$prodPrice", 
                          style: TextStyle(color: Colors.red[700],
                          fontWeight: FontWeight.bold,),
                          ),
                        ),
                      ),
                      new Text("\$$prodOldPrice", 
                      style: TextStyle(color: Colors.grey,
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.lineThrough),
                      ),
                ],
              )
            ),
            child: Image.asset(prodPicture,
            fit: BoxFit.cover),
          ),
          ),
        ),
        ),
        );    
  }
}